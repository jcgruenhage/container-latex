FROM docker.io/alpine:latest
LABEL maintainer="Jan Christian Grünhage <jan.christian@gruenhage.xyz>"

RUN apk add --no-cache texlive-full
